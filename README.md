# nginx-lua-docker


This is repo with multi-stage build Docker image for Nginx with Lua (based on Debian 9).

Example build: `docker build -t nginx-lua:v1 .`

Example run: `docker run -d -v $(pwd)/nginx:/etc/nginx -p 80:80 nginx-lua:v1`
