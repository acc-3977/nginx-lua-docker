FROM debian:9-slim AS build

ENV VER_LUAJIT 2.1-20200102
ENV VER_LUA_NGINX_MODULE 0.10.15
ENV VER_LUA_RESTY_CORE 0.1.17
ENV LUA_LIB_DIR /usr/local/share/lua/5.1
ENV VER_LUA_RESTY_LRUCACHE 0.09
ENV VER_NGINX 1.18.0
ENV VER_NGX_DEVEL_KIT 0.3.1
ENV LUAJIT_LIB /usr/local/lib
ENV LUAJIT_INC /usr/local/include/luajit-2.1
ENV LD_LIBRARY_PATH /usr/local/lib/:$LD_LIBRARY_PATH
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
    ca-certificates \
    libgeoip-dev \
    libpcre3-dev \
    libssl-dev \
    zlib1g-dev \
    curl \
    g++ \
    gzip \
    make \
    tar


RUN curl -Lo /luajit.tar.gz https://github.com/openresty/luajit2/archive/v${VER_LUAJIT}.tar.gz \
    && tar xvzf /luajit.tar.gz \
    && cd /luajit2-${VER_LUAJIT} \
    && make -j "$(nproc)" \
    && make install


RUN curl -Lo /lua-resty-core.tar.gz https://github.com/openresty/lua-resty-core/archive/v${VER_LUA_RESTY_CORE}.tar.gz \
    && tar xvzf /lua-resty-core.tar.gz \
    && cd /lua-resty-core-${VER_LUA_RESTY_CORE} \
    && make -j "$(nproc)" \
    && make install

RUN curl -Lo /lua-resty-lrucache.tar.gz https://github.com/openresty/lua-resty-lrucache/archive/v${VER_LUA_RESTY_LRUCACHE}.tar.gz \
    && tar xvzf /lua-resty-lrucache.tar.gz \
    && cd /lua-resty-lrucache-${VER_LUA_RESTY_LRUCACHE} \
    && make -j "$(nproc)" \
    && make install

RUN curl -Lo /ngx_devel_kit.tar.gz https://github.com/vision5/ngx_devel_kit/archive/v${VER_NGX_DEVEL_KIT}.tar.gz \
    && tar xvzf /ngx_devel_kit.tar.gz

RUN curl -Lo /lua-nginx.tar.gz https://github.com/openresty/lua-nginx-module/archive/v${VER_LUA_NGINX_MODULE}.tar.gz \
      && tar xvzf /lua-nginx.tar.gz

RUN curl -Lo /nginx.tar.gz https://nginx.org/download/nginx-${VER_NGINX}.tar.gz \
    && tar xvzf /nginx.tar.gz \
    && cd /nginx-${VER_NGINX} \
    && ./configure \
        --prefix=/etc/nginx \
        --sbin-path=/usr/sbin/nginx \
        --modules-path=/usr/lib/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --pid-path=/var/run/nginx.pid \
        --lock-path=/var/run/nginx.lock \
        --http-client-body-temp-path=/var/cache/nginx/client_temp \
        --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
        --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
        --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
        --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
        --user=nginx \
        --group=nginx \
        --with-compat \
        --with-file-aio \
        --with-threads \
        --with-http_addition_module \
        --with-http_auth_request_module \
        --with-http_dav_module \
        --with-http_flv_module \
        --with-http_gunzip_module \
        --with-http_gzip_static_module \
        --with-http_mp4_module \
        --with-http_random_index_module \
        --with-http_realip_module \
        --with-http_secure_link_module \
        --with-http_slice_module \
        --with-http_ssl_module \
        --with-http_stub_status_module \
        --with-http_sub_module \
        --with-http_v2_module \
        --with-mail \
        --with-mail_ssl_module \
        --with-stream \
        --with-stream_realip_module \
        --with-stream_ssl_module \
        --with-stream_ssl_preread_module \
        --with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' \
        --with-ld-opt='-Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' \
        --add-module=/lua-nginx-module-${VER_LUA_NGINX_MODULE} \
        --add-module=/ngx_devel_kit-${VER_NGX_DEVEL_KIT} \
        --with-http_dav_module \
        --with-http_geoip_module \
    && make -j "$(nproc)" build \
    && make install

FROM debian:9-slim

ENV LUAJIT_LIB /usr/local/lib
ENV LUAJIT_INC /usr/local/include/luajit-2.1
ENV LD_LIBRARY_PATH /usr/local/lib/:$LD_LIBRARY_PATH

RUN groupadd nginx && useradd --no-create-home nginx -g nginx
RUN mkdir -p /var/cache/nginx/client_temp \
    /var/cache/nginx/proxy_temp \
    /var/cache/nginx/fastcgi_temp \
    /var/cache/nginx/uwsgi_temp \
    /var/cache/nginx/scgi_temp \
    /var/log/nginx

COPY --from=build /usr/sbin/nginx /usr/sbin/nginx
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/include/luajit-2.1 /usr/local/include/luajit-2.1

RUN apt-get update && apt-get install -y libssl-dev libgeoip-dev
RUN chmod +x /usr/sbin/nginx

CMD ["nginx", "-g", "daemon off;"]
